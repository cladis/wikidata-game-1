/********************************************************************************************************************************************
 game - main object
*********************************************************************************************************************************************/

var wdgame = {
//	test_site : ( null != (window.location+'').match(/\/test\b/) ) ,
	thumbsize : 120 ,
	main_screen_languages : [ 'en' ] ,
	first_screen : true ,
	fade_time : 250 ,
	is_logged_into_widar : false ,
	user_name : '' ,
	user_is_bot : false ,
	is_horizontal : true ,
	widar_api : '/widar/index.php?callback=?' ,
	api : './api.php' ,
	wd_api : '//www.wikidata.org/w/api.php?callback=?' ,
	icons : {
		info : '//upload.wikimedia.org/wikipedia/commons/thumb/7/7d/Information_green.svg/120px-Information_green.svg.png' ,
		stats : '//upload.wikimedia.org/wikipedia/commons/thumb/9/9a/Gnome-power-statistics.svg/120px-Gnome-power-statistics.svg.png' ,
		settings : '//upload.wikimedia.org/wikipedia/commons/thumb/c/c2/VisualEditor_-_Icon_-_Settings.svg/120px-VisualEditor_-_Icon_-_Settings.svg.png' ,
		random : '//upload.wikimedia.org/wikipedia/commons/thumb/7/70/Random_font_awesome.svg/120px-Random_font_awesome.svg.png' ,
		merge : '//upload.wikimedia.org/wikipedia/commons/thumb/1/10/Pictogram_voting_merge.svg/120px-Pictogram_voting_merge.svg.png' ,
		person : '//upload.wikimedia.org/wikipedia/commons/thumb/c/c1/Biography_note.svg/120px-Biography_note.svg.png' ,
		nogender : '//upload.wikimedia.org/wikipedia/commons/thumb/6/67/Crystal_Clear_app_Login_Manager.svg/120px-Crystal_Clear_app_Login_Manager.svg.png'
	} ,
	wikipedia_text_cache : {} ,
	mode : '' ,
	modes : {} ,
	lang : 'en' ,
	settings : {
		whitelist:'',
		blacklist:'',
		whitelist_exclusive:false
	} ,

	initGames : function () {
		var self = this ;
		// Start pre-caching
		$.each ( self.modes , function ( k , v ) {
			v.mode = k ; // So the object knows its key
			v.data_cache = [] ;
			v.init() ;
		} ) ;
	} ,
	
	init : function () {
		var self = this ;
		self.is_horizontal = screen.height < screen.width ;
		self.wd = new WikiData() ;
		self.params = self.getUrlVars() ;
		if ( typeof self.params.mode != 'undefined' ) self.mode = self.params.mode ;
		
		$(window).keypress ( function ( e ) {
			var code = e.keyCode || e.which;
			var validCodes = { 40:0, 50:1, 51:2, 97:0, 115:1, 100:2 }; // 1, 2, 3, a, s, d
			if ( typeof self.modes[self.last_mode] == 'undefined' ) return ;
			if ( Object.keys(validCodes).indexOf(code.toString()) == -1 ) return;
			if ( typeof self.modes[self.last_mode].buttons[validCodes[code]] == 'undefined' ) return ;
			$('#'+self.modes[self.last_mode].buttons[code-49]).click() ;
		} ) ;
		
		$(window).keyup ( function ( e ) {
			var code = e.keyCode || e.which;
			if ( typeof self.modes[self.last_mode] == 'undefined' ) return ;
			if ( code < 37 || code > 40 ) return ; // Only arrow keys
			if ( typeof self.modes[self.last_mode].buttons[2-Math.abs(code-39)] == 'undefined' ) return ;
			$('#'+self.modes[self.last_mode].buttons[code-49]).click() ;
		} ) ;
		
		$('#head_title').click ( function () {
			self.mode = '' ;
			self.showMainScreen() ;
		} ) ;

		self.checkOauthStatus ( function () {
			if ( !self.is_logged_into_widar ) {
				self.showMainScreen() ;
			} else {
				self.initGames() ;
				self.loadUserSettings ( function () {
					self.showNextScreen() ;
				} ) ;
			}
		} ) ;
	} ,

	getUrlVars : function () {
		var vars = {} ;
		var hashes = window.location.href.slice(window.location.href.indexOf('#') + 1) ;
		if ( hashes == window.location.href ) hashes = '' ;
		hashes = hashes.split('&');
		$.each ( hashes , function ( i , j ) {
			var hash = j.split('=');
			hash[1] += '' ;
			vars[hash[0]] = decodeURI(hash[1]).replace(/_/g,' ');
		} ) ;
		return vars;
	} ,

	checkOauthStatus : function ( callback ) {
		var self = this ;
		self.is_logged_into_widar = false ;
		$.getJSON ( self.widar_api , {
			action:'get_rights',
			botmode:1
		} , function ( d ) {
			var h = '' ;
			if ( d.error != 'OK' || typeof (d.result||{}).error != 'undefined' ) {
	//			h += "<div><a title='You need to authorise WiDaR to edit on your behalf if you want this tool to edit Wikidata.' target='_blank' href='/widar/index.php?action=authorize'>WiDaR</a><br/>not authorised.</div>" ;
			} else {
				self.is_logged_into_widar = true ;
				self.user_name = d.result.query.userinfo.name ;
	//			h += "<div>Logged into <a title='WiDaR authorised' target='_blank' href='/widar/'>WiDaR</a> as " + d.result.query.userinfo.name + "</div>" ;
				$.each ( d.result.query.userinfo.groups , function ( k , v ) {
					if ( v != 'bot' ) return ;
					self.user_is_bot = true ;
	//				h += "<div><b>You are a bot</b>, no throttling for you!</div>" ;
				} ) ;
				h = "<small>" + self.user_name + "</small>" ;
			}
			$('#widar_state').html ( h ) ;
			if ( typeof callback != 'undefined' ) callback() ;
		} ) ;
	} ,

/*	
	// Obsolete. Keeping around just in case.
	doesExist : function ( q , callback ) { // Does an item exist?
		var self = this ;
		q = self.fixQ ( q ) ;
		$.getJSON ( self.wd_api , {
			action : 'query' ,
			prop : 'info' ,
			titles : q ,
			format : 'json'
		} , function ( data ) {
			$.each ( data.query.pages , function ( k , v ) {
				if ( k == -1 ) {
					callback ( q.replace(/\D/g,'')*1 , false ) ;
				} else {
					callback ( q.replace(/\D/g,'')*1 , true ) ;
				}
			} )
		} ) ;
	} ,
*/
	
	fixQ : function ( q ) {
		return 'Q'+(q+'').replace(/\D/g,'') ;
	} ,

	loadItemSummary : function ( params ) {
		var self = this ;
		var q = self.fixQ ( params.q ) ;
		$(params.target).html ( "<i>Loading item " + q + "...</i>" ) ;
		self.wd.getItemBatch ( [q] , function () {
			var i = self.wd.items[q] ;
			var r = i.raw ;
			var labels = {} ;
			$.each ( (r.labels||{}) , function ( k , v ) {
				if ( typeof labels[v.value] == 'undefined' ) labels[v.value] = 0 ;
				labels[v.value] += 2 ;
			} ) ;
			$.each ( (r.aliases||{}) , function ( k0 , v0 ) {
				$.each ( v0 , function ( k , v ) {
					if ( typeof labels[v.value] == 'undefined' ) labels[v.value] = 0 ;
					labels[v.value] += 1 ;
				} ) ;
			} ) ;
			
			var keys = [] ;
			$.each ( labels , function ( k , v ) { keys.push ( k ) } ) ;
			keys.sort ( function ( a , b ) { return ( labels[b] - labels[a] ) ; } ) ;
			
			var h = '' ;
			var thumb_id = 'thumb_' + params.q ;
			h += "<div style='float:right;margin-left:5px;margin-bottom:2px' id='" +  thumb_id + "'></div>" ;
			h += "<h4>Item <a href='//www.wikidata.org/wiki/" + q + "' target='_blank'>" + q + "</a></h4>" ;
			h += "<div>" ; // Title
			$.each ( keys , function ( dummy , k ) {
				h += "<div class='item_label'>" ;
				if ( typeof params.highlight_label != 'undefined' && params.highlight_label == k ) h += "<b>" + k + "</b>" ;
				else h += k ;
				// h += " (" + labels[k] + "&times;)" ; // Not a "real" count, but weighted
				h += "</div>" ;
			} ) ;
			h += "</div>" ; // Title
			
			h += "<div>" ; // Desc
			$.each ( (r.descriptions||{}) , function ( k , v ) {
				h += "<div class='item_desc'>" + v.value + "</div>" ;
			} ) ;
			h += "</div>" ; // Desc
			
			var props = i.getPropertyList() ;
			var to_load = [] ;
			$.each ( props , function ( dummy0 , p ) {
				to_load.push ( p ) ;
				$.each ( (i.getClaimItemsForProperty(p,true)||[]) , function ( k , v ) { to_load.push(v) } ) ;
			} ) ;
			
			self.wd.getItemBatch ( to_load , function () {

				var sub_h = [] ;
				$.each ( props , function ( dummy0 , p ) {
					var a = i.getStringsForProperty ( p ) ;
					var pre = false ;
					if ( a.length == 0 ) {
						var b = i.getClaimItemsForProperty(p) ;
						$.each ( b , function ( k , v ) {
							a.push ( self.wd.items[v].getLink({target:'_blank'}) ) ;
						} ) ;
						pre = true ;
					}
					if ( a.length == 0 ) {
						$.each ( r.claims[p] , function ( dummy , claim ) {
							var b = i.getClaimDate ( claim ) ;
							if ( typeof b == 'undefined' ) return ;
							var t = b.time ;
							if ( typeof t == 'undefined' ) return ;
							if ( t.substr(0,1) == '-' ) a.push ( '-'+t.substr(8,10) ) ;
							else a.push ( t.substr(8,10) ) ;
						} ) ;
					}
					if ( a.length == 0 ) return ;
					var h2 = '' ;
					h2 += "<div class='item_kv'>" ;
					h2 += "<div class='item_prop'>" + self.wd.items[p].getLabel() + "</div>" ;
					h2 += "<div class='item_prop_values'>" + a.join(", ") + "</div>" ;
					h2 += "</div>" ;
					if ( pre ) sub_h.unshift ( h2 ) ;
					else sub_h.push ( h2 ) ;
				} ) ;
				h += "<div>" + sub_h.join('') + "</div>" ; // Item props, links, string values
				
				h += "<div>" ; // Sitelinks
				var best_lang_num = 999 ;
				var server ;
				var page ;
				var langlist = self.settings.whitelist.replace(/\s/g,'').split(',').concat(self.wd.main_languages) ;
				$.each ( (r.sitelinks||[]) , function ( k , v ) {
					var m = k.match(/^(.+)(wiki)$/) ;
					if ( m == null ) m = k.match(/^(.+)(wik.+)$/) ;
					var tmp_server ;
					if ( m != null ) {
						var lang = m[1] ;
						var project = m[2] ;
						if ( project == 'wiki' ) project = 'wikipedia' ;
						tmp_server = lang + '.' + project + '.org' ;
						if ( project == 'wikipedia' ) {
							if ( best_lang_num == 999 ) {
								server = tmp_server ;
								page = v.title ;
							}
							$.each ( langlist , function ( nml , ml ) {
								if ( ml != lang ) return ;
								if ( nml >= best_lang_num ) return ;
								best_lang_num = nml ;
								server = tmp_server ;
								page = v.title ;
							} ) ;
						}
					}
					h += "<div class='item_sitelink'>" + k + ": " ;
					if ( typeof tmp_server != 'undefined' ) h += "<a target='_blank' href='//"+tmp_server+"/wiki/"+v.title.replace(/'/g,'&39;') + "'>" + v.title + "</a>" ;
					else h += v.title ;
					h += "</div>" ;
				} ) ;
				h += "</div>" ; // Sitelinks
				
				h += "<div class='site_preview'></div>" ;

				$(params.target).html ( h ) ;
				
				var mmf = i.getMultimediaFilesForProperty('P18') ;
				if ( mmf.length > 0 ) {
					$.getJSON ( '//commons.wikimedia.org/w/api.php?callback=?' , {
						action:'query',
						titles:'File:'+mmf[0],
						prop:'imageinfo',
						format:'json',
						iiprop:'url',
						iiurlwidth:self.thumbsize,
						iiurlheight:self.thumbsize
					} , function ( d ) {
						var ii ;
						$.each ( ((d.query||{}).pages||{}) , function ( k , v ) {
							ii = v.imageinfo[0] ;
						} ) ;
						if ( typeof ii == 'undefined' ) return ; // Something's wrong
						var h = "" ;
						h += "<div style='width:"+ii.thumbwidth+"px;height:"+ii.thumbheight+"px'>" ;
						h += "<a target='_blank' href='"+ii.descriptionurl+"'>" ;
						h += "<img border=0 src='" + ii.thumburl + "' />" ;
						h += "</a></div>" ;
						$('#'+thumb_id).html ( h ) ;
					} ) ;
		//			https://commons.wikimedia.org/w/api.php?
					// 
				}
				
				if ( typeof server != 'undefined' ) { // Show preview
					self.loadWikipediaText ( server , page , function ( text ) {
						if ( text == '' ) {
							var h = "<div class='site_preview_title'></div><div class='site_preview_text'></div>" ;
							$(params.target+' div.site_preview').html(h).hide() ;
						} else {
							var h = "<div class='site_preview_title'><b>" + server + ": <a target='_blank' href='//" + server + "/wiki/" + page.replace(/'/g,'&39;') + "'>" + page.replace(/_/g,' ') + "</a></b></div>" ;
							h += "<div class='site_preview_text'>" + text + "</div>" ;
							$(params.target+' div.site_preview').html ( h ) ;
						}
						if ( typeof params.callback != 'undefined' ) params.callback() ;
					} ) ;
				} else {
					if ( typeof params.callback != 'undefined' ) params.callback() ;
				}
				
			} ) ;
			
		} ) ;
	} ,
	
	loadWikipediaText : function ( server , page , callback ) {
		var self = this ;
		var key = server + "::" + page ;
		if ( typeof self.wikipedia_text_cache[key] != 'undefined' ) {
			if ( typeof callback != 'undefined' ) callback ( self.wikipedia_text_cache[key] ) ;
			return ;
		}
		$.getJSON ( '//'+server+'/w/api.php?callback=?' , {
			action:'query',
			prop:'extracts',
			exchars:1000,
//			exsentences:10,
			explaintext:1,
			titles:page ,
			format:'json'
		} , function ( d ) {
			$.each ( ((d.query||{}).pages||{}) , function ( k , v ) {
				self.wikipedia_text_cache[key] = $.trim(v.extract) ;
				if ( typeof callback != 'undefined' ) callback ( self.wikipedia_text_cache[key] ) ;
			} ) ;
		} ) ;
	} ,

	showMainScreen : function () {
		var self = this ;
		
		self.updateHash() ;
		
		// Show main page
		var use_lang = self.lang ;
		if ( -1 == $.inArray ( use_lang , self.main_screen_languages ) ) use_lang = 'en' ;
		
		$.get('main_pages/'+use_lang+'.html?rand='+Math.random(), function(html) {
			$('#main').html ( html ) ;
			$.each ( self.icons , function ( k , v ) {
				$('div.main_page_box[mode="'+k+'"] div.mpb_icon').html ( "<img src='" + v + "'/>" ) ;
			} ) ;
			if ( !self.is_logged_into_widar ) $('#main div.widar_note').show() ;
			var nw = $('#main div.main_page_box').width()-140 ;
			$('#main div.mpb_text').width(nw) ;
			$('#main div.main_page_box').each ( function () {
				var o = $(this) ;
				var mode = o.attr ( 'mode' ) ;
				if ( mode == 'info' ) {
					o.css ( { cursor : 'pointer' } ) ;
					o.click ( function () { o.find('div.wdgame_desc').toggle() } ) ;
					return ;
				}
				if ( !self.is_logged_into_widar ) return ;
				if ( mode == 'stats' || mode == 'settings' ) {
					o.css ( { cursor : 'pointer' } ) ;
					if ( mode == 'stats' ) o.click ( function () { self.setMode('stats') ; self.showStatsScreen() } ) ;
					if ( mode == 'settings' ) o.click ( function () { self.setMode('settings') ; self.showSettingsScreen() } ) ;
					return ;
				}
				if ( mode != 'random' && typeof self.modes[mode] == 'undefined' ) return ;
				o.css ( { cursor : 'pointer' } ) ;
				o.click ( function () {
					self.setMode ( mode ) ;
					self.showNextScreen() ;
				} ) ;
			} ) ;
		} ) ;
		
	} ,
	
	loadUserSettings : function ( callback ) {
		var self = this ;
		$.get ( self.api , {
			action:'get_settings',
			user:self.user_name
		} , function ( d ) {
			if ( d.data != '' ) self.settings = JSON.parse ( d.data ) ;
			if ( typeof callback != 'undefined' ) callback() ;
		} ) ;
	} ,
	

	showSettingsScreen : function () {
		var self = this ;
		$('#main').html ( "<i>Loading settings...</i>" ) ;
		self.loadUserSettings ( function () {
			var hint = 'Language codes, separated by comma' ;
			var h = '' ;
			h += "<h2>User settings</h2>" ;
			h += "<table class='table-condensed table-striped'><tbody>" ;
			h += "<tr><th>Never show languages</th><td><input type='text' class='span3' id='blacklist' value='" + self.settings.blacklist + "' title='"+hint+"' placeholder='"+hint+"' /></td></tr>" ;
			h += "<tr><th>Prefer languages</th><td><input type='text' class='span3' id='whitelist' value='" + self.settings.whitelist + "' title='"+hint+"' placeholder='"+hint+"' /><br/>" ;
			h += "<label><input type='checkbox' id='whitelist_exclusive' "+(self.settings.whitelist_exclusive?'checked':'')+"/> Only use these languages</label> <small>(This can slow down your game experience!)</small></td></tr>" ;
			h += "<tr><td style='text-align:center' colspan=2>" ;
			h += "<button class='btn btn-success' id='save_settings'>Save</button> " ;
			h += "<button class='btn btn-default' id='cancel'>Cancel</button>" ;
			h += "</td></tr>" ;
			h += "</tbody></table>" ;
			
			$('#main').html ( h ) ;
			$('#main label').css({'font-weight':'normal'}) ;
			$('#cancel').click ( function () {
				self.setMode('') ;
				self.showMainScreen() ;
			} ) ;
			$('#save_settings').click ( function () {
				$.each ( ['blacklist','whitelist'] , function ( k , v ) {
					self.settings[v] = $('#'+v).val() ;
				} ) ;
				$.each ( ['whitelist_exclusive'] , function ( k , v ) {
					self.settings[v] = $('#'+v).is(':checked') ? true : false ;
				} ) ;
				$.get ( self.api , {
					action:'set_settings',
					user:self.user_name ,
					settings:JSON.stringify(self.settings)
				} , function ( d ) {
					self.initGames() ;
					self.setMode('');
					self.showMainScreen() ;
				} ) ;
			} ) ;
		} ) ;
	} ,
	
	showStatsScreen : function () {
		var self = this ;
		$('#main').html ( "<i>Loading statistics...</i>" ) ;
		$.get ( self.api , {
			action:'stats',
			user:self.user_name
		} , function ( d ) {
			var perc_factor = 100 ;
			var h = '' ;
			h += "<h2>Game statistics</h2>" ;
			
			h += "<h3>Overview</h3>" ;
			h += "<div class='lead'>Total number of players: " + d.players + "</div>" ;
			h += "<table class='table-condensed table-striped'>" ;
			h += "<thead><tr><th>Game</th><th>Total</th><th>Done</th><th>Done %</th><th>Your score</th><th>Your rank</th></tr></thead>" ;
			h += "<tbody>" ;
			$.each ( d.data , function ( mode , v ) {
				if ( typeof self.modes[mode] == 'undefined' ) return ;
				h += "<tr>" ;
				h += "<th>" + self.modes[mode].name + "</th>" ;
				h += "<td class='num'>" + (v.done_all*1+v.todo*1) + "</td>" ;
				h += "<td class='num'>" + (v.done_all*1) + "</td>" ;
				h += "<td class='num'>" + (Math.floor(v.done_all*100*perc_factor/(v.done_all*1+v.todo*1))/perc_factor).toFixed(2) + "%</td>" ;
				h += "<td class='num'>" + v.your_score + "</td>" ;
				h += "<td class='num'>" + v.rank + "</td>" ;
				h += "</tr>" ;
			} ) ;
			h += "</tbody></table>" ;
			
			h += "<h3>Top 10 players</h3>" ;
			$.each ( d.data , function ( mode , v ) {
				if ( typeof self.modes[mode] == 'undefined' ) return ;
				h += "<h4>Game: <i>" + self.modes[mode].name + "</i></h4>" ;
				h += "<table class='table-condensed table-striped'>" ;
				h += "<thead><tr><th>#</th><th>User</th><th>Score</th></tr></thead>" ;
				h += "<tbody>" ;
				$.each ( v.top10 , function ( k2 , v2 ) {
					var u = v2.user ;
					u = "<a target='_blank' href='//www.wikidata.org/wiki/User:" + encodeURIComponent(u) + "'>" + u + "</a>" ;
					if ( v2.user == self.user_name ) u = "<b>" + u + "</b> (you!)" ;
					h += "<tr><td class='num'>" + (k2+1) + "</td><td>" + u + "</td><td class='num'>" + v2.score + "</td></tr>" ;
				} ) ;
				h += "</tbody></table>" ;
			} ) ;
			
			$('#main').html ( h ) ;
		} ) ;
	} ,
	
	updateHash : function () {
		var self = this ;
		var p = [] ;
		if ( self.mode != '' ) p.push ( 'mode='+self.mode ) ;
		window.location.hash = p.join ( '&' ) ;
	} ,
	
	setMode : function ( mode ) {
		var self = this ;
		self.mode = mode ;
		self.updateHash() ;
	} ,

	
	showNextScreen : function () {
		var self = this ;
		$('#main').html('') ;
		if ( self.first_screen && typeof self.params.mode == 'undefined' ) {
			self.first_screen = false ;
			return self.showMainScreen() ;
		}
		
		var mode = self.mode ;
		if ( mode == 'stats' ) return self.showStatsScreen() ;
		if ( mode == 'settings' ) return self.showSettingsScreen() ;
		if ( mode == '' ) mode = 'random' ;
		if ( mode == 'random' ) {
			var modes = [] ;
			$.each ( self.modes , function ( k , v ) { modes.push ( k ) } ) ;
			var index = Math.floor(Math.random() * modes.length);
			mode = modes[index] ;
		}
		if ( typeof self.modes[mode] == 'undefined' ) {
			return self.showMainScreen() ;
		}
		self.last_mode = mode ;
		self.modes[mode].showNewScreen() ;
		$('#main').fadeIn ( self.fade_time ) ;
	} ,


	addStatementItem : function ( item , prop , target , callback ) {
		$.getJSON ( wdgame.widar_api , {
			action:'set_claims',
			ids:'Q'+(item+'').replace(/\D/g,''),
			prop:'P'+(prop+'').replace(/\D/g,''),
			target:'Q'+(target+'').replace(/\D/g,''),
			botmode:1
		} , function ( d ) {
			if ( typeof callback != 'undefined' ) callback() ;
		} ) ;
	} ,
	
	the_end : true
} ;

/********************************************************************************************************************************************
 generic base object for individual games
*********************************************************************************************************************************************/

var wdgame_generic = {
	data_cache_size : 4 ,
	data_cache : [] ,
	had_id : {} ,
	initialized : false ,
	buttons : [] ,
	
	setItemStatus : function ( data , mode ) {
		var self = this ;
		$('#run_status div[id='+data.id+']').html ( "Remembering..." ) ;
		$.get ( wdgame.api , {
			action:'set_status',
			table:self.mysql_table ,
			user:wdgame.user_name,
			id:data.id,
			status:mode
		} , function ( d ) {
			$('#'+data.id).remove() ;
		} ) ;
	} ,

	showItem : function ( data ) {
		var self = this ;
		var url = wdgame.icons[self.mode] ;
		var h = "<div style='float:right' title='" + self.name + "'><img width='24px' src='" + url + "'></div>" ;
		h += self.getPageStructure ( data ) ;
		if ( data.show ) $('#main').html ( h ) ;
		self.showSections ( data ) ;
		if ( !data.show ) return ; // Pre-caching
		self.addButtonTriggers ( data ) ;
		$.each ( self.buttons , function ( k , v ) {
			$('#'+v).attr ( { title:'Shortcut: '+(k+1) } ) ;
		} ) ;
		$('div.decision button').addClass('btn-lg') ;
		
		if ( !wdgame.is_horizontal ) { // Phone etc.
			$('div.decision').css({position:'fixed',bottom:'0px',left:'0px',right:'0px','z-index':5,margin:'0px'}) ;
			$('div.item_container:last').css({'margin-bottom':'65px'})
		}
	} ,

	highlightButton : function ( name ) {
		var self = this ;
		$('#'+name).css({'font-weight':'bold'}) ;
		$.each ( self.buttons , function ( k , v ) {
			if ( v == name ) return ;
			$('#'+v).attr({disabled:'disabled'}) ;
		} ) ;
	} ,

	getAndShow : function ( show ) {
		var self = this ;
		var data ;
		
		if ( !show && self.data_cache.length >= self.data_cache_size ) return ; // Cache full
		
		if ( show ) {
			while ( self.data_cache.length > 0 && self.data_cache[0].id == 0 ) self.data_cache.shift() ; // Remove dummies
		}
		
		if ( show && self.data_cache.length > 0 ) {
			if ( self.debug ) console.log ( "Using cache" ) ;
			data = self.data_cache.shift() ;
			data.show = true ;
			self.showItem ( data ) ;
			return ;
		}
		if ( self.debug ) {
			if ( show ) console.log ( "Not using cache" ) ;
			else console.log ( "Building cache" ) ;
		}

		if ( show ) $('#main').html ( "<i>" + self.loadingMessage + "</i>" ) ;
		$.get ( wdgame.api , {
			user:wdgame.user_name ,
			action:'get_candidate' ,
			table:self.mysql_table
		} , function ( d ) {
			data = d.data ;
//			data = { id:604155 , item:48632 } ; // TESTING
			if ( typeof self.had_id[data.id] != 'undefined' ) return self.getAndShow ( show ) ;
			self.had_id[data.id] = true ;
			data.show = show ;
			if ( !show ) self.data_cache.push ( data ) ;
			self.showItem ( data ) ;
		} , 'json' ) ;
	} ,

	showNewScreen : function () {
		var self = this ;
		self.getAndShow ( true ) ;
	} ,
	
	init : function () {
		var self = this ;
		if ( self.initialized ) return ;
		self.initialized = true ;
		
		// Pre-cache one item, fill rest of cache with dummies
		while ( self.data_cache.length + 1 < self.data_cache_size ) self.data_cache.push ( { id:0 } ) ;
		self.getAndShow ( false ) ;
	} ,
	
	the_end : true
}

/********************************************************************************************************************************************
 nogender
*********************************************************************************************************************************************/

wdgame.modes['nogender'] = $.extend ( true , {}, {

//	debug:true,
	name : 'Gender' ,
	mysql_table : 'genderless_people' ,
	loadingMessage : 'Looking for random gender-less person data...' ,
	buttons : ['male','dunno','female'] ,

	getPageStructure : function ( data ) {
		var self = this ;
		var h = '' ;
		h += "<div>This item represents a person, but has no sex/gender property.</div>" ;
		h += "<div id='data' class='item_container'></div>" ;
		h += "<div class='decision'>" ;
		h += "<div class='btn-group'>" ;
		h += "<button id='male' class='btn btn-success'>Male</button> " ;
		h += "<button id='dunno' class='btn btn-default' accesskey='x'>Not sure</button> " ;
		h += "<button id='female' class='btn btn-primary'>Female</button> " ;
		h += "</div>" ;
		h += "</div>" ;
		return h ;
	} ,
	showSections : function ( data ) {
		var self = this ;
		wdgame.loadItemSummary ( {q:data.item , target:data.show?'#data':'#nope' , callback:function () {
			self.getAndShow ( false ) ;
			if ( !data.show ) return ;
			$('div.site_preview_text').each ( function () {
				var o = $(this) ;
				var t = o.html() ;
				t = t.replace ( /\b(he|him|his|she|her)\b/gi , "<span class='highlight'>\$1</span>" ) ; // EN
				t = t.replace ( /\b(er|sie|sein|seines|ihr|ihres)\b/gi , "<span class='highlight'>\$1</span>" ) ; // DE
				t = t.replace ( /\b(il|elle|un|une|son|sa)\b/gi , "<span class='highlight'>\$1</span>" ) ; // FR
				t = t.replace ( /\b(hij|ze|zij|zijn|hem|haar)\b/gi , "<span class='highlight'>\$1</span>" ) ; // NL
				t = t.replace ( /\b(él|lo|un|ella|la|una)\b/gi , "<span class='highlight'>\$1</span>" ) ; // ES
				t = t.replace ( /\b(ele|ela|um|uma)\b/gi , "<span class='highlight'>\$1</span>" ) ; // PT
				t = t.replace ( /\b(он|она|сын|дочь|племянник|племянница)\b/gi , "<span class='highlight'>\$1</span>" ) ; // RU
				t = t.replace ( /(ский|ская)\b/gi , "<span class='highlight'>\$1</span>" ) ; // RU endings
				t = t.replace ( /\b(він|вона|син|дочка|племінник|племінниця)\b/gi , "<span class='highlight'>\$1</span>" ) ; // UK
				t = t.replace ( /(ський|ська)\b/gi , "<span class='highlight'>\$1</span>" ) ; // UK endings
				
				o.html ( t ) ;
			} ) ;
		} } ) ;
	} ,
	addButtonTriggers : function ( data ) {
		var self = this ;
		$('#male').click ( function () {
			self.highlightButton ( $(this).attr('id') ) ;
			$('#main').fadeOut ( wdgame.fade_time , function () { wdgame.showNextScreen() ; } ) ;
			$('#run_status').append ( "<div id='"+data.id+"'>Marking Q"+data.item+" as male...</div>" ) ;
			wdgame.addStatementItem ( data.item , 'P21' , 'Q6581097' , function () { self.setItemStatus(data,'MALE') } ) ;
		} ) ;
		$('#female').click ( function () {
			self.highlightButton ( $(this).attr('id') ) ;
			$('#main').fadeOut ( wdgame.fade_time , function () { wdgame.showNextScreen() ; } ) ;
			$('#run_status').append ( "<div id='"+data.id+"'>Marking Q"+data.item+" as female...</div>" ) ;
			wdgame.addStatementItem ( data.item , 'P21' , 'Q6581072' , function () { self.setItemStatus(data,'FEMALE') } ) ;
		} ) ;
		$('#dunno').click ( function () {
			self.highlightButton ( $(this).attr('id') ) ;
			$('#main').fadeOut ( wdgame.fade_time , function () {
				wdgame.showNextScreen() ;
			} ) ;
		} ) ;
	}

} , wdgame_generic ) ;


/********************************************************************************************************************************************
 person
*********************************************************************************************************************************************/

wdgame.modes['person'] = $.extend ( true , {}, {

//	debug:true,
	name : 'Person' ,
	mysql_table : 'potential_people' ,
	loadingMessage : 'Looking for random item that could be a person...' ,
	buttons : ['person','dunno','no_person'] ,

	getPageStructure : function ( data ) {
		var self = this ;
		var h = '' ;
		h += "<div>This item has no \"instance of\", and could be a person.</div>" ;
		h += "<div id='data' class='item_container'></div>" ;
		h += "<div class='decision'>" ;
		h += "<div class='btn-group'>" ;
		h += "<button id='person' class='btn btn-success'>Person</button> " ;
		h += "<button id='dunno' class='btn btn-default' accesskey='x'>Not sure</button> " ;
		h += "<button id='no_person' class='btn btn-primary'>Not a person</button> " ;
		h += "</div>" ;
		h += "</div>" ;
		return h ;
	} ,
	showSections : function ( data ) {
		var self = this ;
		wdgame.loadItemSummary ( {q:data.item , target:data.show?'#data':'#nope' , callback:function () {
			self.getAndShow ( false ) ;
		} } ) ;
	} ,
	addButtonTriggers : function ( data ) {
		var self = this ;
		$('#person').click ( function () {
			self.highlightButton ( $(this).attr('id') ) ;
			$('#main').fadeOut ( wdgame.fade_time , function () { wdgame.showNextScreen() ; } ) ;
			$('#run_status').append ( "<div id='"+data.id+"'>Marking Q"+data.item+" as person...</div>" ) ;
			wdgame.addStatementItem ( data.item , 'P31' , 'Q5' , function () { self.setItemStatus(data,'YES') } ) ;
		} ) ;
		$('#no_person').click ( function () {
			self.highlightButton ( $(this).attr('id') ) ;
			$('#main').fadeOut ( wdgame.fade_time , function () { wdgame.showNextScreen() ; } ) ;
			self.setItemStatus(data,'NO') ;
		} ) ;
		$('#dunno').click ( function () {
			self.highlightButton ( $(this).attr('id') ) ;
			$('#main').fadeOut ( wdgame.fade_time , function () {
				wdgame.showNextScreen() ;
			} ) ;
		} ) ;
	}

} , wdgame_generic ) ;


/********************************************************************************************************************************************
 merge
*********************************************************************************************************************************************/
/*
wdgame.modes['merge'] = $.extend ( true , {}, {

//	debug:true,
	name : 'Merge items' ,
	mysql_table : 'item_pairs' ,
	loadingMessage : 'Looking for random merge candidate pair...' ,
	buttons : ['same','dunno','different'] ,

	getPageStructure : function ( data ) {
		var h = '' ;
		h += "<h3 style='margin-top:0px'>" + data['label'] + "</h3>" ;
		h += "<div><small>These two Wikidata items share this label</small></div>" ;
		h += "<div id='item2' class='item_container'></div>" ;
		h += "<div class='decision'>" ;
		h += "<div class='btn-group'>" ;
		h += "<button id='same' class='btn btn-success'>Same topic</button> " ;
		h += "<button id='dunno' class='btn btn-default' accesskey='x'>Not sure</button> " ;
		h += "<button id='different' class='btn btn-primary'>Different</button> " ;
		h += "</div>" ;
		h += "</div>" ;
		h += "<div id='item1' class='item_container'></div>" ;
		return h ;
	} ,
	showSections : function ( data ) {
		var self = this ;
		var summary_loaded = 0 ;
		function summaryLoaded () {
			summary_loaded++ ;
			if ( summary_loaded < 2 ) return ;
			self.getAndShow ( false ) ;
		}
		
		wdgame.loadItemSummary ( {q:data.item1 , target:data.show?'#item1':'#nope' , highlight_label:data.label , callback:summaryLoaded } ) ;
		wdgame.loadItemSummary ( {q:data.item2 , target:data.show?'#item2':'#nope' , highlight_label:data.label , callback:summaryLoaded } ) ;
	} ,
	addButtonTriggers : function ( data ) {
		var self = this ;
		
		$('#same').click ( function () {
			self.highlightButton ( $(this).attr('id') ) ;
			$('#main').fadeOut ( wdgame.fade_time , function () {
				self.doMergeItems ( data ) ;
			} ) ;
		} ) ;
		$('#different').click ( function () {
			self.highlightButton ( $(this).attr('id') ) ;
			$('#main').fadeOut ( wdgame.fade_time , function () {
				self.setItemStatus(data,'DIFF') ;
				wdgame.showNextScreen() ;
			} ) ;
		} ) ;
		$('#dunno').click ( function () {
			self.highlightButton ( $(this).attr('id') ) ;
			$('#main').fadeOut ( wdgame.fade_time , function () {
				wdgame.showNextScreen() ;
			} ) ;
		} ) ;
	} ,
	doMergeItems : function ( data ) {
		var self = this ;
		$('#run_status').append ( "<div id='"+data.id+"'>Merging Q"+data.item2+" into Q"+data.item1+"...</div>" ) ;
		$.getJSON ( wdgame.widar_api , {
			action:'merge_items',
			from:'Q'+data.item2,
			to:'Q'+data.item1,
			botmode:1
		} , function ( d ) {
			$('#run_status div[id='+data.id+']').html ( "Requesting deletion for Q"+data.item2 ) ;
			var row = "{{subst:Rfd|1=Q"+data.item2+"|2=Merged into [[Q"+data.item1+"]]}} --~~~~" ;
			$.getJSON ( wdgame.widar_api , {
				action:'add_row',
				page:'Wikidata:Requests for deletions',
				row:row,
				botmode:1
			} , function ( d ) {
				self.setItemStatus(data,'SAME') ;
			} ) ;
		} ) ;
		wdgame.showNextScreen() ;
	}

} , wdgame_generic ) ;
*/

/********************************************************************************************************************************************
 startup
*********************************************************************************************************************************************/

$(document).ready ( function () {
	wdgame.init() ;
} ) ;
